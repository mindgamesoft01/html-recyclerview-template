package com.mindgame.htmlrecyclerviewtemplate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;


/**
 * Created by kalyani on 9/5/2017.
 */

public class Webview_Activity extends Activity {
    String[]html={"file:///android_asset/index1.html","file:///android_asset/index2.html","file:///android_asset/index3.html","file:///android_asset/index4.html",
            "file:///android_asset/index5.html","file:///android_asset/index6.html","file:///android_asset/index7.html","file:///android_asset/index8.html","file:///android_asset/index9.html",
            "file:///android_asset/index10.html","file:///android_asset/index11.html","file:///android_asset/index12.html","file:///android_asset/index13.html"};
    WebView webView;
    int position;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    int scale;

    singleton_images sc=singleton_images.getInstance();
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page ="ca-app-pub-4951445087103663/9343069725";
    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 ="ca-app-pub-4951445087103663/9343069725";
     ProgressDialog pd ;
    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();
    final AdRequest adRequest = new AdRequest.Builder().build();
    String app_status = sc.app_mode();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        webView=(WebView)findViewById(R.id.webview);
        WebSettings setting = webView.getSettings();

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);
        pd=new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);


        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);


        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);







        setting.setJavaScriptEnabled(true);
        setting.setBuiltInZoomControls(true);
        setting.setSupportZoom(true);
        webView.setInitialScale(0);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        this.webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.setScrollbarFadingEnabled(false);
        Bundle b= getIntent().getExtras();
        position=b.getInt("flag");
        webView.loadUrl(html[position]);


    }




        @Override
        public boolean onKeyDown ( int keyCode, KeyEvent event) {
            if (app_status.equals("PROD")) {
//        super.onBackPressed();
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }

            }
            return super.onKeyDown(keyCode, event);
        }
    }



