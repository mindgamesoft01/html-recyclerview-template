package com.mindgame.htmlrecyclerviewtemplate;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by kalyani on 8/7/2017.
 */

public class NetworkStatusCheck  {


    private static final NetworkStatusCheck ourInstance = new NetworkStatusCheck();

    public static NetworkStatusCheck getInstance() {
        return ourInstance;
    }

    private NetworkStatusCheck() {
    }

   public Boolean network_status;
    public Boolean json_status;


    public boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            network_status = (netInfo != null && netInfo.isConnected());
            Log.e("NETWORK", network_status.toString());
            return network_status;
        } catch (NullPointerException e) {
            e.printStackTrace();
            network_status = Boolean.FALSE;
            return network_status;
        }
    }
}
