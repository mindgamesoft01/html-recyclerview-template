package com.mindgame.htmlrecyclerviewtemplate;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by lenovo on 16-Apr-17.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private String []  loadingimags;
    private Context context;

    public DataAdapter(Context context, String []  loading_imags) {
        this.context = context;
        this.loadingimags = loading_imags;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

       /* Glide.with(context).load(loadingimags.get(i))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.img);*/
       // viewHolder.img.setImageURI(loadingimags.get(i));
   //     Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
      //  Picasso.with(context).load(loadingimags.get(i).getImage_url()).into(viewHolder.img);

        //Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
        viewHolder.img.setText(loadingimags[i]);

    }

    @Override
    public int getItemCount() {
        return loadingimags.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView img;
        public ViewHolder(final View view) {
            super(view);

            img= (TextView) view.findViewById(R.id.img1);


            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //new AdListner().AdMobInterstitial(v.getContext(),ImageviewActivity.class,getPosition());
                    Intent i=new Intent(v.getContext(), Webview_Activity.class);
                    i.putExtra("images",loadingimags);
                    i.putExtra("POS",getAdapterPosition());
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                    //  context.startActivity(new Intent(v.getContext(), ImageviewActivity.class).putExtra("POS", getPosition()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        }

        @Override
        public void onClick(View v) {

        }
    }
}