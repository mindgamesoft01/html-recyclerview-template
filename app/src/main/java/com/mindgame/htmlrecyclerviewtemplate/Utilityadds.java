package com.mindgame.htmlrecyclerviewtemplate;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class Utilityadds {

    Context c;
    LinearLayout layout, layout1;
    Activity activity;

    // AdMOb
    AdView adView;
    String AD_UNIT_ID = "ca-app-pub-4951445087103663/3901768296";

    AdView adView1;
    String AD_UNIT_ID1 = "ca-app-pub-4951445087103663/3901768296";


    InterstitialAd interstitial;
    String AD_UNIT_ID_full_page = "ca-app-pub-4951445087103663/6407075341";

    InterstitialAd interstitial1;
    String AD_UNIT_ID_full_page1 = "ca-app-pub-4951445087103663/6407075341";


    public LinearLayout layout_strip(Context context) {

        c = context;
        Log.d("TASG", "entered layout code");

        layout = new LinearLayout(c);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        return layout;
    }

    public void AdMobBanner(Context c) {

        adView = new AdView(c);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(AD_UNIT_ID);

        layout.addView(adView);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

    }

    public void AdMobBanner1(Context c) {

        adView1 = new AdView(c);
        adView1.setAdSize(AdSize.SMART_BANNER);
        adView1.setAdUnitId(AD_UNIT_ID1);

        layout.addView(adView1);

        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

    }

    public void AdMobInterstitial(final Context c, final Class destination) {


        if (NetworkStatusCheck.getInstance().isOnline(c) == true) {
            interstitial = new InterstitialAd(c);
            interstitial.setAdUnitId(AD_UNIT_ID_full_page);

            final ProgressDialog pd = new ProgressDialog(c);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Please wait ..." + "Add Loading ...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();

            // Create ad request.
            AdRequest adRequest = new AdRequest.Builder().build();
            // Begin loading your interstitial.
            interstitial.loadAd(adRequest);
            interstitial.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // TODO Auto-generated method stub
                    super.onAdLoaded();
                    pd.dismiss();
                    interstitial.show();

                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    pd.dismiss();
                    c.startActivity(new Intent(c, destination));

                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    pd.dismiss();
                    c.startActivity(new Intent(c, destination));

                }
            });

        } else {

            c.startActivity(new Intent(c, destination));


        }
    }


    public void AdMobInterstitial1(final Context c, final Class destination) {


        if (NetworkStatusCheck.getInstance().isOnline(c) == true) {
            interstitial1 = new InterstitialAd(c);
            interstitial1.setAdUnitId(AD_UNIT_ID_full_page1);

            final ProgressDialog pd = new ProgressDialog(c);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage("Please wait ..." + "Add Loading ...");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();

            // Create ad request.
            AdRequest adRequest = new AdRequest.Builder().build();
            // Begin loading your interstitial.
            interstitial1.loadAd(adRequest);
            interstitial1.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // TODO Auto-generated method stub
                    super.onAdLoaded();
                    pd.dismiss();
                    interstitial1.show();

                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    pd.dismiss();
                    c.startActivity(new Intent(c, destination));

                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    pd.dismiss();
                    c.startActivity(new Intent(c, destination));

                }
            });

        } else {

            c.startActivity(new Intent(c, destination));


        }
    }
    public void AdMobInterstitia_context(final Context c) {


        if (NetworkStatusCheck.getInstance().isOnline(c) == true) {
            interstitial = new InterstitialAd(c);
            interstitial.setAdUnitId(AD_UNIT_ID_full_page);


            // Create ad request.
            AdRequest adRequest = new AdRequest.Builder().build();
            // Begin loading your interstitial.
            interstitial.loadAd(adRequest);
            interstitial.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // TODO Auto-generated method stub
                    super.onAdLoaded();
                    interstitial.show();

                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    //c.startActivity(new Intent(c.this));

                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);

                }
            });

        } else {

            //c.startActivity(new Intent(c, destination));


        }
    }


}

