package com.mindgame.htmlrecyclerviewtemplate;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class AdClass {

	Context c;
	LinearLayout layout, layout1;
	// AdMOb
	AdView adView;
	String AD_UNIT_ID = "ca-app-pub-4951445087103663/2375621529";

	AdView adView1;
	String AD_UNIT_ID1 = "ca-app-pub-4951445087103663/2375621529";

	InterstitialAd interstitial;
	String AD_UNIT_ID_full_page = "ca-app-pub-4951445087103663/6977921957";

	InterstitialAd interstitial1;
	String AD_UNIT_ID_full_page1 = "ca-app-pub-4951445087103663/6977921957";


	//for banner: ca-app-pub-3940256099942544/6300978111
	//for interstitial: ca-app-pub-3940256099942544/1033173712


	

	/*
	 * // RevMob RevMob revmob; RevMobBanner banner; // StartApp private
	 * StartAppAd startAppAd;
	 * 
	 * // MM Ads
	 * 
	 * MM mm; MMBestApps mmb
	 */;

	public LinearLayout layout_strip(Context context) {

		c = context;
		Log.d("TASG", "entered layout code");

		layout = new LinearLayout(c);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		layout.setLayoutParams(params);
		layout.setOrientation(LinearLayout.HORIZONTAL);

		return layout;
	}

	public void AdMobBanner(Context c) {

		adView = new AdView(c);
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId(AD_UNIT_ID);

		layout.addView(adView);

		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

	}

	public void AdMobBanner1(Context c) {

		adView1 = new AdView(c);
		adView1.setAdSize(AdSize.SMART_BANNER);
		adView1.setAdUnitId(AD_UNIT_ID1);

		layout.addView(adView1);

		AdRequest adRequest1 = new AdRequest.Builder().build();
		adView1.loadAd(adRequest1);

	}

	public void AdMobInterstitial(Context c) {
		interstitial = new InterstitialAd(c);
		interstitial.setAdUnitId(AD_UNIT_ID_full_page);

		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();
		// Begin loading your interstitial.
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				interstitial.show();
			}
		});

	}

	public void AdMobInterstitial1(Context c) {
		interstitial1 = new InterstitialAd(c);
		interstitial1.setAdUnitId(AD_UNIT_ID_full_page1);

		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();
		// Begin loading your interstitial.
		interstitial1.loadAd(adRequest);
		interstitial1.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				interstitial1.show();
			}
		});

	}

}
